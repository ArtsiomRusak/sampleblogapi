﻿using System;
using System.Threading.Tasks;
using SampleBlog.BusinessModels.Models;
using SampleBlog.Infrastructure.Mapping;
using SampleBlog.RepositoryContracts.Contracts;
using SampleBlog.RepositoryContracts.Models;
using SampleBlog.ServiceContracts.Contracts;

namespace SampleBlog.Services.Services
{
  public class MembershipService : IMembershipService
  {
    private readonly IMembershipRepository _membershipRepository;
    private readonly IMapper<User, UserDto> _userMapper;

    public MembershipService(IMembershipRepository membershipRepository, IMapper<User, UserDto> userMapper)
    {
      _membershipRepository = membershipRepository;
      _userMapper = userMapper;
    }

    public async Task<bool> VerifyUser(int userId)
    {
      throw new System.NotImplementedException();
    }

    public async Task<User> RegisterUser(User user)
    {
      var check = await this.VerifyUser(user.Id);
      if (check == false)
      {
        return null;
      }

      var userDto = _userMapper.Map(user);
      var registeredUser = await _membershipRepository.CreateEntity(userDto);

      if (registeredUser == null)
      {
        return null;
      }

      var result = _userMapper.Map(registeredUser);
      return result;
    }

    public async Task<User> RegisterUser(string email, string password)
    {
      throw new System.NotImplementedException();
    }

    public async Task<bool> LoginUser(string email, string password)
    {
      return true;
    }

    public async Task<bool> LogoutUser(string email)
    {
      throw new NotImplementedException();
    }

    public async Task<User> GetUser(int userId)
    {
      throw new System.NotImplementedException();
    }
  }
}