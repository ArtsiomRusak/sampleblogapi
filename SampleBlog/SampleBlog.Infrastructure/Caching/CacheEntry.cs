﻿namespace SampleBlog.Infrastructure.Caching
{
  internal class CacheEntry
  {
    public string Key { get; set; }

    public object Value { get; set; }
  }
}