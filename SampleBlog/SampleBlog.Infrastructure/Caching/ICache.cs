﻿namespace SampleBlog.Infrastructure.Caching
{
  public interface ICache
  {
    void AddEntry(string key, object value);
  }
}