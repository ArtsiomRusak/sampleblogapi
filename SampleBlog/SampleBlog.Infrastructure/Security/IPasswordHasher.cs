﻿namespace SampleBlog.Infrastructure.Security
{
  public interface IPasswordHasher
  {
    string Hash(string password, string passwordSalt);
  }
}
