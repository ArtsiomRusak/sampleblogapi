﻿namespace SampleBlog.Infrastructure.Mapping
{
  public interface IMapper<T, TV>
  {
    T Map(TV model);
    TV Map(T model);
  }
}