﻿using SampleBlog.RepositoryContracts.Models;

namespace SampleBlog.RepositoryContracts.Contracts
{
  public interface IMembershipRepository : IBaseRepository<UserDto, int>
  {
  }
}