﻿using System.Threading.Tasks;
using SampleBlog.RepositoryContracts.Models;

namespace SampleBlog.RepositoryContracts.Contracts
{
  public interface IBaseRepository<TModel, TKeyType> : IRepository
    where TModel : Entity
    where TKeyType : struct
  {
    Task<TModel> CreateEntity(TModel model);

    Task UpdateEntity(TModel model);

    Task DeleteEntity(TKeyType id);

    Task<TModel> GetEntityById(TKeyType id);
  }
}