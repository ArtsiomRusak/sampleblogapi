﻿using System.Threading.Tasks;
using SampleBlog.RepositoryContracts.Contracts;
using SampleBlog.RepositoryContracts.Models;

namespace SampleBlog.Repository.Repositories
{
  public abstract class Repository<TModel, TKeyType> : IBaseRepository<TModel, TKeyType>
    where TModel : Entity, new()
    where TKeyType : struct
  {
    public virtual async Task<TModel> CreateEntity(TModel model)
    {
      return null;
    }

    public virtual async Task UpdateEntity(TModel model)
    {
      //base updating
    }

    public virtual async Task DeleteEntity(TKeyType id)
    {
      //base deleting
    }

    public virtual async Task<TModel> GetEntityById(TKeyType id)
    {
      //base getting

      return null;
    }
  }
}