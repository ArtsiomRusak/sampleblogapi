﻿using System;

namespace SampleBlog.Repository.UnitOfWork
{
  public interface IUnitOfWork : IDisposable
  {
    void Commit();
  }
}
