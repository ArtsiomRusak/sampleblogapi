﻿using System.Threading.Tasks;
using SampleBlog.BusinessModels.Models;

namespace SampleBlog.ServiceContracts.Contracts
{
  public interface IMembershipService : IService
  {
    Task<bool> VerifyUser(int userId);

    Task<User> RegisterUser(User user);

    Task<User> RegisterUser(string email, string password);

    Task<bool> LoginUser(string email, string password);

    Task<bool> LogoutUser(string email);

    Task<User> GetUser(int userId);
  }
}