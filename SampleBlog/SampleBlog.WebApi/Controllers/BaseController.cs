﻿using System.Web.Http;

namespace SampleBlog.WebApi.Controllers
{
  [RoutePrefix("api/v1")]
  public class BaseController : ApiController
  {
  }
}