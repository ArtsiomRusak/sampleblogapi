﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using SampleBlog.WebApi.Models;

namespace SampleBlog.WebApi.Controllers
{
  public class PostController : BaseController
  {
    [Route("search/posts")]
    [HttpGet]
    public async Task<IHttpActionResult> SearchPosts([FromUri]SearchApiModel model)
    {
      var posts = new List<PostApiModel>();
      for (int i = 0; i < model.Pages; i++)
      {
        posts.Add(
          new PostApiModel
          {
            Id = i + 1,
            Title = model.Query + model.Pages + 1
          });
      }

      return this.Ok(posts);
    }

    [Route("post/{slug:int}")]
    [HttpGet]
    public async Task<IHttpActionResult> GetPost([FromUri] string fields)
    {
      return null;
    }

    [Route("post/{slug:int}/comment")]
    [HttpPost]
    public async Task<IHttpActionResult> AddPostComment([FromBody] string message)
    {
      return null;
    }
  }
}