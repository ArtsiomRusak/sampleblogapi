﻿using System.Threading.Tasks;
using System.Web.Http;
using SampleBlog.BusinessModels.Models;
using SampleBlog.Infrastructure.Mapping;
using SampleBlog.ServiceContracts.Contracts;
using SampleBlog.WebApi.Models;

namespace SampleBlog.WebApi.Controllers
{
  public class MembershipController : BaseController
  {
    private readonly IMembershipService _membershipService;
    private readonly IMapper<UserApiModel, User> _mapper;

    public MembershipController(IMembershipService membershipService, IMapper<UserApiModel, User> mapper)
    {
      _membershipService = membershipService;
      _mapper = mapper;
    }

    [Route("signup")]
    [HttpPost]
    public async Task<IHttpActionResult> SignUp(UserApiModel user)
    {
      //validation: IValidatableObject, attribute validation
      if (!this.ModelState.IsValid)
      {
        return this.BadRequest(this.ModelState);
      }

      var mappedUser = _mapper.Map(user);
      var registeredUser = await _membershipService.RegisterUser(mappedUser);
      return this.Ok(registeredUser);
    }

    [Route("signin")]
    [HttpGet]
    public async Task<IHttpActionResult> SingIn([FromUri]UserApiModel model)
    {
      if (!this.ModelState.IsValid)
      {
        return this.BadRequest(this.ModelState);
      }

      var login = await _membershipService.LoginUser(model.Email, model.Password);
      return this.Ok(login);
    }

    [Route("signout")]
    [HttpPost]
    public async Task<IHttpActionResult> SignOut(UserApiModel model)
    {
      if (!this.ModelState.IsValid)
      {
        return this.BadRequest(this.ModelState);
      }

      var logout = await _membershipService.LogoutUser(model.Email);
      return this.Ok(logout);
    }

    [Route("user")]
    [HttpGet]
    public async Task<IHttpActionResult> GetUser(int id)
    {
      if (!this.ModelState.IsValid)
      {
        return this.BadRequest(this.ModelState);
      }

      var user = await _membershipService.GetUser(id);
      return this.Ok(user);
    }
  }
}