﻿namespace SampleBlog.WebApi.Models
{
  public class SearchApiModel
  {
    public string Query { get; set; }

    public int Pages { get; set; }
  }
}