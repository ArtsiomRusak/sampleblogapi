﻿namespace SampleBlog.WebApi.Models
{
  public class PostApiModel
  {
    public int Id { get; set; }

    public string Title { get; set; }
  }
}