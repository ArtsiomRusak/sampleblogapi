﻿using System.Reflection;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Routing;
using SampleBlog.WebApi.Controllers;

namespace SampleBlog.WebApi.WebAPIRouteProviders
{
  public class CustomDirectRouteProvider : DefaultDirectRouteProvider
  {
    protected override string GetRoutePrefix(HttpControllerDescriptor controllerDescriptor)
    {
      var routePrefix = base.GetRoutePrefix(controllerDescriptor);
      var controllerBaseType = controllerDescriptor.ControllerType.BaseType;
      if (controllerBaseType == typeof(BaseController))
      {
        routePrefix = controllerBaseType.GetCustomAttribute<RoutePrefixAttribute>().Prefix + (string.IsNullOrEmpty(routePrefix) == false ? "/" + routePrefix : routePrefix);
      }

      return routePrefix;
    }
  }
}