﻿using System.Linq;
using System.Web.Http;
using SampleBlog.WebApi.WebAPIRouteProviders;

namespace SampleBlog.WebApi
{
  public static class WebApiConfig
  {
    public static void Register(HttpConfiguration config)
    {
      // Web API configuration and services

      // Web API routes
      config.MapHttpAttributeRoutes(new CustomDirectRouteProvider());

      var appXmlType = config.Formatters.XmlFormatter.SupportedMediaTypes.FirstOrDefault(t => t.MediaType == "application/xml");
      config.Formatters.XmlFormatter.SupportedMediaTypes.Remove(appXmlType);
    }
  }
}
