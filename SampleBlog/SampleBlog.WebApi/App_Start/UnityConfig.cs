using System.Web.Http;
using Microsoft.Practices.Unity;
using SampleBlog.BusinessModels.Models;
using SampleBlog.Infrastructure.Mapping;
using SampleBlog.Repository.Repositories;
using SampleBlog.RepositoryContracts.Contracts;
using SampleBlog.RepositoryContracts.Models;
using SampleBlog.ServiceContracts.Contracts;
using SampleBlog.Services.Services;
using SampleBlog.WebApi.Mappers;
using SampleBlog.WebApi.Models;
using Unity.WebApi;

namespace SampleBlog.WebApi
{
  public static class UnityConfig
  {
    public static void RegisterComponents()
    {
      var container = new UnityContainer();

      container.RegisterType<IMembershipRepository, MembershipRepository>();
      container.RegisterType<IMembershipService, MembershipService>();
      container.RegisterType<IMapper<UserApiModel, User>, UserMapper>();
      container.RegisterType<IMapper<User, UserDto>, SampleBlog.ServiceContracts.Mappers.UserMapper>();

      GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
    }
  }
}